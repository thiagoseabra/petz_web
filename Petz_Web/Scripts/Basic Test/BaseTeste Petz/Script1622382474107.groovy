import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import javax.swing.JOptionPane

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable
import net.bytebuddy.implementation.bytecode.Throw

import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(url)

WebUI.setText( findTestObject('Object Repository/Petz Main Page/edt_Busca') , "ração")

WebUI.click( findTestObject('Object Repository/Petz Main Page/btn_Buscar') , FailureHandling.STOP_ON_FAILURE)

WebUI.click( findTestObject('Object Repository/Petz Main Page/Produto_3') , FailureHandling.STOP_ON_FAILURE)

String ProdutoNome = WebUI.getText( findTestObject('Object Repository/Produto Page/txt_NomeProduto') , FailureHandling.STOP_ON_FAILURE)

String ProdutoMarca = WebUI.getText( findTestObject('Object Repository/Produto Page/txt_Marca') , FailureHandling.STOP_ON_FAILURE)

String ProdutoPreco = WebUI.getText( findTestObject('Object Repository/Produto Page/txt_PrecoNormal') , FailureHandling.STOP_ON_FAILURE)

String ProdutoPrecoAssinante = WebUI.getText( findTestObject('Object Repository/Produto Page/txt_PrecoAssinante') , FailureHandling.STOP_ON_FAILURE)

if(WebUI.verifyElementPresent( findTestObject('Object Repository/Politica Petz/txt_Politica de Privacidade'),10)) {
	
	WebUI.click(findTestObject('Object Repository/Politica Petz/btn_Continuar')  )
	
}

WebUI.click( findTestObject('Object Repository/Produto Page/btn_Carrinho') , FailureHandling.STOP_ON_FAILURE)

if(!ProdutoNome.equalsIgnoreCase(WebUI.getText( findTestObject('Object Repository/Carrinho Page/txt_ProdutoCarrinho') ))) {
	JOptionPane.showMessageDialog(null, "Houve Divergencia nos valores:\nNome do Produto:"+ProdutoNome+"\nNome Exibido no Carrinho:"+WebUI.getText( findTestObject('Object Repository/Carrinho Page/txt_ProdutoCarrinho') ))
	WebUI.closeBrowser()
	throw new Error();
	}

if (!ProdutoPreco.equalsIgnoreCase(WebUI.getText(findTestObject('Object Repository/Carrinho Page/txt_PrecoCarrinho'))) ) 
	{
	JOptionPane.showMessageDialog(null, "Houve Divergencia nos valores:\nValor do Produto:"+ProdutoPreco+"\nValor Exibido no Carrinho:"+WebUI.getText(findTestObject('Object Repository/Carrinho Page/txt_PrecoCarrinho')))
	WebUI.closeBrowser()
	throw new Error();
	}
	
WebUI.closeBrowser()	