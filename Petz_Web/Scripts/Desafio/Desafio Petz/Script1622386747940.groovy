import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import javax.swing.JOptionPane

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUICommonScripts
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable
import io.netty.handler.codec.http.HttpHeaders.Values

import org.openqa.selenium.Keys as Keys

ArrayList<String> listProdutos = new ArrayList<String>()


Produto Racao1 = new Produto("Ração Fórmula Golden para Cães Adultos sabor Peru e Arroz 15kg")
listProdutos.add(Racao1)
Produto Racao2 = new Produto("Ração Golden Fórmula Light para Cães Adultos - 15kg")
listProdutos.add(Racao2)
Produto Racao3 = new Produto("Caneca Dosadora Petz para Cães e Gatos Azul")
listProdutos.add(Racao3)
Produto Racao4 = new Produto("Ração Premier Raças Específicas Golden Retriever para Cães Adultos - 12kg")
listProdutos.add(Racao4)
Produto Racao5 = new Produto("Ração Golden Power Training para Cães Adultos Sabor Frango e Arroz - 15kg")
listProdutos.add(Racao5)		



for(i=0; i<listProdutos.size(); i++ )
{
	Produto t = listProdutos.get(i)
	
WebUI.openBrowser(url)

WebUI.maximizeWindow()

WebUI.setText( findTestObject('Object Repository/Petz Main Page/edt_Busca') , "ração")

WebUI.click( findTestObject('Object Repository/Petz Main Page/btn_Buscar') , FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'BaseFunctions.ClickText'(t.getNome())

t.setNome(WebUI.getText(findTestObject('Object Repository/Produto Page/txt_NomeProduto')))

t.setMarca(WebUI.getText( findTestObject('Object Repository/Produto Page/txt_Marca')))

t.setValorNormal(WebUI.getText( findTestObject('Object Repository/Produto Page/txt_PrecoNormal')))

t.setValorAssinante(WebUI.getText( findTestObject('Object Repository/Produto Page/txt_PrecoAssinante')))

if(WebUI.verifyElementPresent( findTestObject('Object Repository/Politica Petz/txt_Politica de Privacidade'),10)) {
	
	WebUI.click(findTestObject('Object Repository/Politica Petz/btn_Continuar')  )
	
}

WebUI.click( findTestObject('Object Repository/Produto Page/btn_Carrinho') , FailureHandling.STOP_ON_FAILURE)

if(!t.getNome().equalsIgnoreCase(WebUI.getText( findTestObject('Object Repository/Carrinho Page/txt_ProdutoCarrinho') ))) {
	JOptionPane.showMessageDialog(null, "Houve Divergencia nos valores:\nNome do Produto:"+t.getNome()+"\nNome Exibido no Carrinho:"+WebUI.getText( findTestObject('Object Repository/Carrinho Page/txt_ProdutoCarrinho') ))
	WebUI.closeBrowser()
	throw new Error();
	}

if (!t.getValorNormal().equalsIgnoreCase(WebUI.getText(findTestObject('Object Repository/Carrinho Page/txt_PrecoCarrinho'))) )
	{
	JOptionPane.showMessageDialog(null, "Houve Divergencia nos valores:\nValor do Produto:"+t.getValorNormal()+"\nValor Exibido no Carrinho:"+WebUI.getText(findTestObject('Object Repository/Carrinho Page/txt_PrecoCarrinho')))
	WebUI.closeBrowser()
	throw new Error();
	}

	
WebUI.closeBrowser()
}