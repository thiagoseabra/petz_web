<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Produto_3</name>
   <tag></tag>
   <elementGuidId>57325d0e-c6cb-46d4-9064-412ef96330e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[@id='produto-href'])[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/produto/racao-golden-formula-senior-para-caes-adultos-sabor-frango-e-arroz-15kg-71353</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>petzProduct</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>produto-href</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>itemprop</name>
      <type>Main</type>
      <value>itemListElement</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>itemtype</name>
      <type>Main</type>
      <value>http://schema.org/Product</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>clickEvent(this); return true;</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-idproduto</name>
      <type>Main</type>
      <value>71353</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-nomeproduto</name>
      <type>Main</type>
      <value>Ração Golden Fórmula Senior para Cães Adultos Sabor Frango e Arroz - 15kg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-precoproduto</name>
      <type>Main</type>
      <value>150.9</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						{&quot;price&quot;:&quot;150.90&quot;,&quot;name&quot;:&quot;Ração Golden Fórmula Senior para Cães Adultos Sabor Frango e Arroz - 15kg&quot;,&quot;id&quot;:&quot;71353&quot;,&quot;sku&quot;:&quot;71353&quot;,&quot;category&quot;:&quot;Ração Seca&quot;,&quot;brand&quot;:&quot;Premier Pet&quot;,&quot;hideSubscriberDiscountPrice&quot;:false}
		

		
		
		
			
		
		
			
			
		
		
			
				
				
			
		
		
			
			
				
			
			
				
				
				
			
			
		

		
		
			
		
		
			
		
		
		Ração Golden Fórmula Senior para Cães Adultos Sabor Frango e Arroz - 15kg
		
			
				
				
					
					
						
					
				
			

			
				

				
					
						R$ 150,90
					
					
				
				
					
				
			
			
		
		
			
				
			
			
				
					
						R$ 135,81
					
				
				10% OFF
			
			
				para assinantes
			
		
	</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gridProdutos&quot;)/li[@class=&quot;liProduct&quot;]/a[@id=&quot;produto-href&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//a[@id='produto-href'])[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='gridProdutos']/li[3]/a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='para assinantes'])[2]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='R$ 14,39'])[2]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/produto/racao-golden-formula-senior-para-caes-adultos-sabor-frango-e-arroz-15kg-71353')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/a[2]</value>
   </webElementXpaths>
</WebElementEntity>
